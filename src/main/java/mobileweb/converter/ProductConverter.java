package mobileweb.converter;

import org.springframework.stereotype.Component;

import mobileweb.dto.ProductDTO;
import mobileweb.entity.ProductEntity;

@Component
public class ProductConverter {
	public ProductEntity toEntity(ProductDTO dto) {
		ProductEntity productEntity = new ProductEntity();
		productEntity.setName(dto.getName());
		productEntity.setPrice(dto.getPrice());
		productEntity.setImage(dto.getImage());
		productEntity.setRating(dto.getRating());
		productEntity.setDescription(dto.getDescription());
		return productEntity;
	}

	
	public ProductDTO toDto(ProductEntity entity) {
		ProductDTO productDTO = new ProductDTO();
		if(entity.getId() !=null) {
			productDTO.setId(entity.getId());
		}
		productDTO.setName(entity.getName());
		productDTO.setPrice(entity.getPrice());
		productDTO.setImage(entity.getImage());
		productDTO.setRating(entity.getRating());
		productDTO.setDescription(entity.getDescription());
		return productDTO;
	}
	
	public ProductEntity toEntity(ProductDTO dto, ProductEntity entity) {
		
		entity.setName(dto.getName());
		entity.setPrice(dto.getPrice());
		entity.setImage(dto.getImage());
		entity.setRating(dto.getRating());
		entity.setDescription(dto.getDescription());
		return entity;
	}
}
