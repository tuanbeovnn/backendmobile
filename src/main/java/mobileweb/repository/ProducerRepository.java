package mobileweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mobileweb.entity.ProducerEntity;

public interface ProducerRepository extends JpaRepository<ProducerEntity, Long> {
	ProducerEntity findByCode(String code);
}
